const express = require('express')
    , route = express.Router()
    , jwt = require('jsonwebtoken')
    , Users = require('../schema/user')
    , dotenv = require('dotenv')
    , ObjectId = require('mongoose').Types.ObjectId
    , GoogleAuth = require('google-auth-library')
    , auth = new GoogleAuth
    , client = new auth.OAuth2(process.env.GOOGLE_CLIENT_ID, '', '');

dotenv.config();

function generateJWT(user) {
    return jwt.sign({
        _id: user._id
    }, process.env.JWT_SECRET)
}

route.post('/', (req, res) => {
    client.verifyIdToken(
        req.body.token,
        process.env.GOOGLE_CLIENT_ID,
        (e, login) => {
            let payload = login.getPayload();
            if (payload['hd'] !== 'kiit.ac.in') return res.json({ status: 'err', msg: 'You\'re not allowed.' });
            Users.findOne({
                gid: payload['sub']
            }).lean().exec((err, user) => {
                if (err) return res.json({status: 'err', msg: err});
                if (user) {
                    Users.findOneAndUpdate({
                        _id: ObjectId(user._id)
                    }, {
                        fcm: req.body.fcmToken
                    }, {
                        new: true
                    }).lean().exec((err, updatedUser) => {
                        if (err) return res.json({status: 'err', msg: err});
                        res.json({
                            status: 'ok',
                            log: 11,
                            body: updatedUser,
                            token: generateJWT(updatedUser)
                        })
                    })
                } else {
                    Users({
                        gid: payload['sub'],
                        domain: payload['hd'],
                        school: 'KIIT University',
                        firstname: payload['given_name'],
                        lastname: payload['family_name'],
                        email: payload['email'],
                        fcm: req.body.fcmToken,
                        profilephoto: payload['picture']
                    }).save((err, newUser) => {
                        if (err) return res.json({status: 'err', msg: err});
                        res.json({
                            status: 'ok',
                            log: 21,
                            user: newUser,
                            token: generateJWT(newUser)
                        })
                    });
                }
            })
        });
});

route.get('/fetch', (req, res) => {
    jwt.verify(req.headers.authorization, process.env.JWT_SECRET, function (err, user) {
        if (err) throw ('No Authorized User Found');
        Users.findOne({_id: user._id}).lean().exec((err, authUser) => {
            if (err) throw err;
            res.json({
                user: authUser
            })
        })
    })
})

module.exports = route;