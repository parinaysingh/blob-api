const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
    gid: {
        type: String,
        required: true
    },
    domain: {
        type: String,
        required: true
    },
    school: {
        type: String
    },
    firstname: {
        type: String,
        required: true
    },
    lastname: {
        type: String
    },
    email: {
        type: String,
        required: true
    },
    karma: {
        type: Number,
        default: 0
    },
    fcm: {
        type: String
    },
    profilephoto: {
        type: String,
        required: true
    },
}, {
    timestamps: true
}, {
    collection: 'users'
});

module.exports = mongoose.model('user', userSchema);